import React, { Component } from "react";
import {
  MetricChart,
  CurrencyFormatter,
  PercentageFormatter,
  NumberAbbrFormatter
} from "ChoozleUI";
import moment from "moment";
import { hourly } from "./data";

class Hourly extends Component {
  formatTooltip(value, name) {
    if (
      name === "spend" ||
      name === "cpm" ||
      name === "cpc" ||
      name === "cpa"
    ) {
      return <CurrencyFormatter amount={value} />;
    } else if (name === "ctr") {
      return <PercentageFormatter amount={value} precision={3} />;
    } else if (name === "winrate") {
      return <PercentageFormatter amount={value} precision={2} />;
    } else {
      return <NumberAbbrFormatter amount={value} />;
    }
  }

  formatXAxis(tickItem) {
    return moment(tickItem).format("YYYY-MM-DD hh:mm");
  }

  yTickFormatter(value) {
    let base = Math.floor(Math.log(Math.abs(value)) / Math.log(1000));
    let pseudoMetric = "kMBQ"[base - 1];
    return pseudoMetric
      ? parseFloat(value / Math.pow(1000, base)).toFixed(2) + pseudoMetric
      : "" + value;
  }

  render() {
    console.log(moment("2018-08-01 10:00").format("YYYY-MM-DD hh:mm"));
    return (
      <div style={{ marginBotton: "60px" }}>
        <h3>Conversions By Ad Group Hourly </h3>
        <MetricChart
          name="adGroup1"
          name2="adGroup2"
          data={hourly}
          yLabel={{
            value: "Conversions",
            angle: -90,
            position: "center",
            marginBottom: "5px"
          }}
          yLabel2={{
            value: "",
            angle: -90,
            position: "center",
            marginBottom: "5px"
          }}
          xHeight={50}
          formatTooltip={this.formatTooltip}
          dataKey="adGroup1"
          dataKey2="adGroup2"
          gradient="blue"
          gradient2="green"
        />
      </div>
    );
  }
}

export default Hourly;
