import React, { Component } from "react";
import {
  MetricChart,
  CurrencyFormatter,
  PercentageFormatter,
  NumberAbbrFormatter
} from "ChoozleUI";
import moment from "moment";

class ExampleMetricChart extends Component {
  formatTooltip(value, name) {
    if (
      name === "spend" ||
      name === "cpm" ||
      name === "cpc" ||
      name === "cpa"
    ) {
      return <CurrencyFormatter amount={value} />;
    } else if (name === "ctr") {
      return <PercentageFormatter amount={value} precision={3} />;
    } else if (name === "winrate") {
      return <PercentageFormatter amount={value} precision={2} />;
    } else {
      return <NumberAbbrFormatter amount={value} />;
    }
  }

  formatXAxis(tickItem) {
    return moment(tickItem).format("M.DD.YYYY");
  }

  yTickFormatter(value) {
    let base = Math.floor(Math.log(Math.abs(value)) / Math.log(1000));
    let pseudoMetric = "kMBQ"[base - 1];
    return pseudoMetric
      ? parseFloat(value / Math.pow(1000, base)).toFixed(2) + pseudoMetric
      : "" + value;
  }

  render() {
    return (
      <div style={{ marginBotton: "60px" }}>
        <MetricChart
          name="impressions"
          name2="spend"
          data={chartData}
          yLabel={{
            value: "Impressions",
            angle: -90,
            position: "center",
            marginBottom: "5px"
          }}
          yLabel2={{
            value: "Spend",
            angle: -90,
            position: "center",
            marginBottom: "5px"
          }}
          xHeight={50}
          formatTooltip={this.formatTooltip}
          dataKey="impressions"
          gradient="blue"
        />
        <MetricChart
          name="Impressions"
          data={chartData}
          yLabel={{
            value: "Impressions",
            angle: -90,
            position: "center",
            marginBottom: "5px"
          }}
          xHeight={50}
          formatTooltip={this.formatTooltip}
          dataKey="impressions"
          gradient="orange"
        />
        <MetricChart
          name="Impressions"
          name2="Spend"
          data={chartData}
          yLabel={{
            value: "Impressions",
            angle: -90,
            position: "center",
            marginBottom: "5px"
          }}
          yLabel2={{
            value: "Spend",
            angle: -90,
            position: "center",
            marginBottom: "5px"
          }}
          xHeight={50}
          formatTooltip={this.formatTooltip}
          dataKey="impressions"
          dataKey2="spend"
          gradient="orange"
          gradient2="purple"
        />
        <MetricChart
          name="Impressions"
          name2="Win Rate"
          data={chartData}
          yLabel={{
            value: "Impressions",
            angle: -90,
            position: "center",
            marginBottom: "5px"
          }}
          yLabel2={{
            value: "Win Rate",
            angle: -90,
            position: "center",
            marginBottom: "5px"
          }}
          xHeight={50}
          formatTooltip={this.formatTooltip}
          dataKey="impressions"
          gradient="blue"
          dataKey2="winrate"
          gradient2="green"
        />
      </div>
    );
  }
}

export default ExampleMetricChart;

const chartData = [
  {
    name: "Time 1",
    spend: 4324,
    winrate: 9070,
    impressions: 1440,
    date: "2018-05-11"
  },
  {
    name: "Time 2",
    spend: 3333,
    winrate: 7222,
    impressions: 1840,
    date: "2018-05-13"
  },
  {
    name: "Time 3",
    spend: 7860,
    winrate: 10222,
    impressions: 7440,
    date: "2018-05-14"
  },
  {
    name: "Time 4",
    spend: 1223,
    winrate: 5412,
    impressions: 2446,
    date: "2018-05-15"
  },
  {
    name: "Time 5",
    spend: 1890,
    winrate: 3298,
    impressions: 3440,
    date: "2018-05-16"
  },
  {
    name: "Time 6",
    spend: 0,
    winrate: 4503,
    impressions: 9480,
    date: "2018-05-17"
  },
  {
    name: "Time 7",
    spend: 1490,
    winrate: 1209,
    impressions: 3440,
    date: "2018-05-18"
  }
];
