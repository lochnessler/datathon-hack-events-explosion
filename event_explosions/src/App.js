import React, { Component } from "react";
import logo from "./logo.svg";
import "./App.css";
import FirstChart from "./FirstChart";
import Creatives from "./Creatives";
import Hourly from "./Hourly";

class App extends Component {
  render() {
    return (
      <div className="App">
        <FirstChart />
        <Creatives />
        <Hourly />
      </div>
    );
  }
}

export default App;
