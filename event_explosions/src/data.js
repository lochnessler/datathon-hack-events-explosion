export const data = [
  {
    accountId: "116",
    cpm: 7.960000038146973,
    bidCpm: 5,
    campaignId: "1231",
    clicks: 2,
    requests: 6,
    impressions: 4,
    creativeId: "1311",
    clickCpm: 1.9800000190734863,
    date: "2018-05-11"
  },
  {
    accountId: "123",
    cpm: 7.559999942779541,
    bidCpm: 0,
    campaignId: "123",
    clicks: 0,
    requests: 5,
    impressions: 4,
    creativeId: "123",
    clickCpm: 0,
    date: "2018-05-12"
  },
  {
    accountId: "116",
    cpm: 0,
    bidCpm: 0,
    campaignId: "1231",
    clicks: 1,
    requests: 10,
    impressions: 0,
    creativeId: "1311",
    clickCpm: 0.9900000095367432,
    date: "2018-05-13"
  },
  {
    accountId: "66644",
    cpm: 3.7799999713897705,
    bidCpm: 2.5,
    campaignId: "1166",
    clicks: 2,
    requests: 2,
    impressions: 2,
    creativeId: "9977",
    clickCpm: 1.9800000190734863,
    date: "2018-05-14"
  },
  {
    accountId: "123",
    cpm: 7.139999866485596,
    bidCpm: 8.75,
    campaignId: "123",
    clicks: 2,
    requests: 8,
    impressions: 7,
    creativeId: "123",
    clickCpm: 1.9800000190734863,
    date: "2018-05-15"
  },
  {
    accountId: "123",
    cpm: 5.099999904632568,
    bidCpm: 6.25,
    campaignId: "123",
    clicks: 0,
    requests: 10,
    impressions: 5,
    creativeId: "123",
    clickCpm: 0,
    date: "2018-05-16"
  },
  {
    accountId: "123",
    cpm: 7.139999866485596,
    bidCpm: 8.75,
    campaignId: "123",
    clicks: 0,
    requests: 11,
    impressions: 11,
    creativeId: "123",
    clickCpm: 0,
    date: "2018-05-17"
  }
  // {
  //   accountId: "123",
  //   cpm: 2.0399999618530273,
  //   bidCpm: 2.5,
  //   campaignId: "123",
  //   clicks: 0,
  //   requests: 4,
  //   impressions: 2,
  //   creativeId: "123",
  //   clickCpm: 0
  // },
  // {
  //   accountId: "123",
  //   cpm: 11.220000267028809,
  //   bidCpm: 13.75,
  //   campaignId: "123",
  //   clicks: 0,
  //   requests: 18,
  //   impressions: 11,
  //   creativeId: "123",
  //   clickCpm: 0
  // },
  // {
  //   accountId: "123",
  //   cpm: 15.300000190734863,
  //   bidCpm: 18.75,
  //   campaignId: "123",
  //   clicks: 0,
  //   requests: 28,
  //   impressions: 15,
  //   creativeId: "123",
  //   clickCpm: 0
  // },
  // {
  //   accountId: "116",
  //   cpm: 23.459999084472656,
  //   bidCpm: 28.75,
  //   campaignId: "123",
  //   clicks: 30,
  //   requests: 29,
  //   impressions: 23,
  //   creativeId: "123",
  //   clickCpm: 29.700000762939453
  // },
  // {
  //   accountId: "116",
  //   cpm: 14.280000686645508,
  //   bidCpm: 17.5,
  //   campaignId: "123",
  //   clicks: 27,
  //   requests: 15,
  //   impressions: 14,
  //   creativeId: "123",
  //   clickCpm: 26.729999542236328
  // },
  // {
  //   accountId: "116",
  //   cpm: 6.119999885559082,
  //   bidCpm: 7.5,
  //   campaignId: "123",
  //   clicks: 1,
  //   requests: 9,
  //   impressions: 6,
  //   creativeId: "123",
  //   clickCpm: 0.9900000095367432
  // },
  // {
  //   accountId: "116",
  //   cpm: 7.139999866485596,
  //   bidCpm: 8.75,
  //   campaignId: "123",
  //   clicks: 5,
  //   requests: 8,
  //   impressions: 7,
  //   creativeId: "123",
  //   clickCpm: 4.949999809265137
  // },
  // {
  //   accountId: "116",
  //   cpm: 5.139999866485596,
  //   bidCpm: 6.75,
  //   campaignId: "234",
  //   clicks: 15,
  //   requests: 15,
  //   impressions: 17,
  //   creativeId: "234",
  //   clickCpm: 6.949999809265137
  // },
  // {
  //   accountId: "116",
  //   cpm: 3.139999866485596,
  //   bidCpm: 2.75,
  //   campaignId: "234",
  //   clicks: 11,
  //   requests: 10,
  //   impressions: 20,
  //   creativeId: "234",
  //   clickCpm: 5.949999809265137
  // },
  // {
  //   accountId: "116",
  //   cpm: 4.139999866485596,
  //   bidCpm: 1.75,
  //   campaignId: "234",
  //   clicks: 110,
  //   requests: 10,
  //   impressions: 120,
  //   creativeId: "234",
  //   clickCpm: 7.949999809265137
  // },
  // {
  //   accountId: "116",
  //   cpm: 2.139999866485596,
  //   bidCpm: 1.75,
  //   campaignId: "234",
  //   clicks: 33,
  //   requests: 25,
  //   impressions: 50,
  //   creativeId: "234",
  //   clickCpm: 3.949999809265137
  // }
];

export const creatives = [
  {
    creative1: 6,
    creative2: 3,
    date: "2018-05-11"
  },
  {
    creative1: 9,
    creative2: 5,
    date: "2018-05-12"
  },
  {
    creative1: 10,
    creative2: 8,
    date: "2018-05-13"
  },
  {
    creative1: 11,
    creative2: 7,
    date: "2018-05-14"
  },
  {
    creative1: 8,
    creative2: 6,
    date: "2018-05-15"
  },
  {
    creative1: 4,
    creative2: 10,
    date: "2018-05-16"
  },
  {
    creative1: 3,
    creative2: 15,
    date: "2018-05-17"
  }
];

export const conversions = [
  {
    adGroup1: 9,
    adGroup2: 9,
    date: "2018-05-11"
  },
  {
    adGroup1: 6,
    adGroup2: 5,
    date: "2018-05-12"
  },
  {
    adGroup1: 7,
    adGroup2: 7,
    date: "2018-05-13"
  },
  {
    adGroup1: 7,
    adGroup2: 8,
    date: "2018-05-14"
  },
  {
    adGroup1: 6,
    adGroup2: 9,
    date: "2018-05-15"
  },
  {
    adGroup1: 6,
    adGroup2: 10,
    date: "2018-05-16"
  },
  {
    adGroup1: 5,
    adGroup2: 15,
    date: "2018-05-17"
  }
];

export const hourly = [
  {
    adGroup1: 9,
    adGroup2: 9,
    date: "2018-08-01 05:00"
  },
  {
    adGroup1: 6,
    adGroup2: 5,
    date: "2018-08-01 06:00"
  },
  {
    adGroup1: 7,
    adGroup2: 7,
    date: "2018-08-01 07:00"
  },
  {
    adGroup1: 7,
    adGroup2: 8,
    date: "2018-08-01 08:00"
  },
  {
    adGroup1: 6,
    adGroup2: 9,
    date: "2018-08-01 09:00"
  },
  {
    adGroup1: 6,
    adGroup2: 10,
    date: "2018-08-01 10:00"
  },
  {
    adGroup1: 5,
    adGroup2: 15,
    date: "2018-08-01 11:00"
  }
];

export const dsps = [
  {
    dsp1: 6,
    dsp2: 6,
    date: "2018-05-11"
  },
  {
    dsp1: 2,
    dsp2: 2,
    date: "2018-05-12"
  },
  {
    dsp1: 7,
    dsp2: 3,
    date: "2018-05-13"
  },
  {
    dsp1: 4,
    dsp2: 7,
    date: "2018-05-14"
  },
  {
    dsp1: 8,
    dsp2: 8,
    date: "2018-05-15"
  },
  {
    dsp1: 4,
    dsp2: 5,
    date: "2018-05-16"
  },
  {
    dsp1: 0,
    dsp2: 0,
    date: "2018-05-17"
  }
];
