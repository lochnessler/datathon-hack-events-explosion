import React, { Component } from "react";
import {
  MetricChart,
  CurrencyFormatter,
  PercentageFormatter,
  NumberAbbrFormatter
} from "ChoozleUI";
import moment from "moment";
import { data, creatives, conversions, dsps } from "./data";

class ExampleMetricChart extends Component {
  formatTooltip(value, name) {
    if (
      name === "spend" ||
      name === "cpm" ||
      name === "cpc" ||
      name === "cpa"
    ) {
      return <CurrencyFormatter amount={value} />;
    } else if (name === "ctr") {
      return <PercentageFormatter amount={value} precision={3} />;
    } else if (name === "winrate") {
      return <PercentageFormatter amount={value} precision={2} />;
    } else {
      return <NumberAbbrFormatter amount={value} />;
    }
  }

  formatXAxis(tickItem) {
    return moment(tickItem).format("M.DD.YYYY");
  }

  yTickFormatter(value) {
    let base = Math.floor(Math.log(Math.abs(value)) / Math.log(1000));
    let pseudoMetric = "kMBQ"[base - 1];
    return pseudoMetric
      ? parseFloat(value / Math.pow(1000, base)).toFixed(2) + pseudoMetric
      : "" + value;
  }

  render() {
    return (
      <div style={{ marginBotton: "60px" }}>
        <h3>Requests vs. Impressions for Event Explosion Campaign</h3>
        <MetricChart
          name="requests"
          name2="impressions"
          data={data}
          yLabel={{
            value: "Requests",
            angle: -90,
            position: "center",
            marginBottom: "5px"
          }}
          yLabel2={{
            value: "Impressions",
            angle: -90,
            position: "center",
            marginBottom: "5px"
          }}
          xHeight={50}
          formatTooltip={this.formatTooltip}
          dataKey="requests"
          dataKey2="impressions"
          gradient="blue"
          gradient2="green"
        />
        <h3>Clicks by Creative</h3>
        <MetricChart
          name="Clicks for Creative 1"
          name2="Clicks for Creative 2"
          data={creatives}
          yLabel={{
            value: "Clicks",
            angle: -90,
            position: "center",
            marginBottom: "5px"
          }}
          yLabel2={{
            value: "",
            angle: -90,
            position: "center",
            marginBottom: "5px"
          }}
          xHeight={50}
          formatTooltip={this.formatTooltip}
          dataKey="creative1"
          dataKey2="creative2"
          gradient="blue"
          gradient2="green"
        />
        <h3>Conversion by Ad Group</h3>
        <MetricChart
          name="Conversions for Ad Group 1"
          name2="Conversions for Ad Group 2"
          data={conversions}
          yLabel={{
            value: "Conversions",
            angle: -90,
            position: "center",
            marginBottom: "5px"
          }}
          yLabel2={{
            value: "",
            angle: -90,
            position: "center",
            marginBottom: "5px"
          }}
          xHeight={50}
          formatTooltip={this.formatTooltip}
          dataKey="adGroup1"
          dataKey2="adGroup2"
          gradient="blue"
          gradient2="green"
        />
        <h3>Performance TTD vs. AppNexus</h3>
        <MetricChart
          name="Conversions for TTD"
          name2="Conversions for AppNexus"
          data={dsps}
          yLabel={{
            value: "Conversions",
            angle: -90,
            position: "center",
            marginBottom: "5px"
          }}
          yLabel2={{
            value: "",
            angle: -90,
            position: "center",
            marginBottom: "5px"
          }}
          xHeight={50}
          formatTooltip={this.formatTooltip}
          dataKey="dsp1"
          dataKey2="dsp2"
          gradient="blue"
          gradient2="green"
        />
      </div>
    );
  }
}

export default ExampleMetricChart;
